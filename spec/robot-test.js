var Robot = require('../routes/robot');
var assert = require("chai").assert;

var Facing = {
    North: 'NORTH',
    South: 'SOUTH',
    East: 'EAST',
    West: 'WEST'
};

describe('Robot', function() {
    it('Initialised But Not Placed Robot Cannot Be Moved', function() {
        var robot = new Robot();
        var success = robot.Move();
        assert.equal(success, false);
    });

    it('Initialised But Not Placed Robot Cannot Be Turned', function() {
        var robot = new Robot();
        var success = robot.Left();
        assert.equal(success, false);
    });

    it('Initialised But Not Placed Robot Cannot Report Its Position', function() {
        var robot = new Robot();
        var position = robot.Report();
        assert.equal(position,'Robot needs to be placed on the table');
    });

    it('Robot Cannot Be Placed Off Table', function() {
        var robot = new Robot();
        var success = robot.Place(-1, 0, Facing.North);
        assert.equal(success, false);
    });

    /*it('Robot When Placed Can Report Its Position', function() {
        var robot = new Robot();
        var success = robot.Place(3, 2, Facing.East);
        var position = robot.Report();
        // console.log(position);
        assert.equal(success, true); 
        assert.equal(position, '3 2 EAST');
    });*/
});