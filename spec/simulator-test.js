var Simulator = require('../routes/simulator');
var Robot = require('../routes/robot');
var assert = require('chai').assert;

describe('Simulator', function() {
    it('Robot Simulator Can Be Initialised Properly', function() {
        var simulator = new Simulator(new Robot());
        assert.isObject(simulator);
        assert.instanceOf(simulator,Simulator);
    });

    it('Empty Command Is Reported Invalid', function() {
        var simulator = new Simulator(new Robot());
        var response = simulator.Command('');
        assert.equal(response, false);
    });

    it('Unrecognized Command Is Reported', function() {
        var simulator = new Simulator(new Robot());
        var response = simulator.Command('TEST');
        assert.equal(response, false);
    });

    it('Recognized Command But Is Reported Invalid', function() {
        var simulator = new Simulator(new Robot());
        var response = simulator.Command('MOVE');
        assert.equal(response, false);
    });

    it('PLACE Command With No Arguments Report', function() {
        var simulator = new Simulator(new Robot());
        var response = simulator.Command('PLACE');
        assert.equal(response, false);
    });    

    it('PLACE Command With Invalid Arguments', function() {
        var simulator = new Simulator(new Robot());
        var response = simulator.Command('PLACE YYYYY');
        assert.equal(response, false);
        var response = simulator.Command('PLACE 1,x,EAST');
        assert.equal(response, false);
        var response = simulator.Command('PLACE Y,1,SOUTH');
        assert.equal(response, false);
        var response = simulator.Command('PLACE 1,1,EASTWEST');
        assert.equal(response, false);        
    });

   /* it('Robot Placed And Move in Correct Position', function() {
        var simulator = new Simulator(new Robot());
        var response= simulator.Command('PLACE 0, 0, NORTH');
        simulator.Command('MOVE');
         assert.equal(response, true);
         assert.equal(simulator.Command('REPORT'), '0, 1, NORTH');
        // assert.equal(success('REPORT'));
    });*/
});