'use strict'
	

var Simulator = require('./routes/simulator');
var Robot = require('./routes/robot');
var fs = require('fs');

var options = {};
var args = process.argv.slice(2);

if (args && args[0] === 'f') {
	if (args[1] && fs.existsSync(args[1]))
		options = { input: fs.createReadStream(args[1]) }			
	else
		console.log('file not find ', args[1]);
}

var simulator = new Simulator(new Robot(), true, options);

