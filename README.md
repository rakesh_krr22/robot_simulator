# robot_simulator

* The application is a simulation of a robot moving on a square table, of dimensions 5 units x 5 units.
* Robo simulator is a Cli Application
* The robot is free to move in the table,but must be prevented from falling,however valid movement commands must still be allowed. 

# Dependencies

* Nodejs
* npm 

# Setup
* install nodejs and npm 
* Clone the repository using git clone https://rakesh_krr22@bitbucket.org/rakesh_krr22/robot_simulator.git
* cd robot_simulator
* Run npm install ,it will download necessary dependencies

# Run Application

* The application run in two ways.
* In the project directory console enter the node server.js
* Passing the instructions from file use node server.js f fileName.txt
###examples:
   *  node server.js
   * node server.js f test-data/sample-com1.txt
   * node server.js f test-data/sample-com2.txt
   * node server.js f test-data/sample-com3.txt
   

# Operating Instructions
* The application can read in commands of the following form -

* PLACE X,Y,F
* MOVE
* LEFT
* RIGHT
* REPORT

# Run Test Cases
* Run test cases with :
* npm test  spec/simulator-test.js